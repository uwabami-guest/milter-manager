<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE refentry 
  PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
  "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd">
<refentry id="install-to-centos">
<refmeta>
  <refentrytitle role='top_of_page' id='install-to-centos.top_of_page'>Install to CentOS 7</refentrytitle>
  <refmiscinfo>MILTER-MANAGER Library</refmiscinfo>
</refmeta>
<refnamediv>
  <refname>Install to CentOS 7</refname>
  <refpurpose>How to install milter manager to CentOS 7</refpurpose>
</refnamediv>
<refsect1>
  <title>About this document</title>
  <para>
  This document describes how to install milter manager to CentOS 7. See 
  <link linkend='install'>Install</link>
   for general install information.
</para>
  <para>In this document, CentOS 7.6 is used. Sudo is used to run a command with root privilege. If you don&#39;t use sudo, use su instead.</para>
</refsect1>

<refsect1>
  <title>Install packages</title>
  <para>Postfix is used as MTA because it&#39;s installed by default.</para>
  <para>Spamass-milter, clamav-milter and milter-greylist are used as milters. Milter packages registered in EPEL are used.</para>
  <para>Register EPEL like the following:</para>
  <programlisting>% sudo yum install -y epel-release</programlisting>
  <para>Now, you install milters:</para>
  <programlisting>% sudo yum install -y spamass-milter-postfix clamav-scanner-systemd clamav-update clamav-milter clamav-milter-systemd milter-greylist</programlisting>
  <para>And you install RRDtool for generating graphs:</para>
  <programlisting>% sudo yum install -y rrdtool</programlisting>
</refsect1>

<refsect1>
  <title>Build and Install</title>
  <para>milter manager can be installed by yum.</para>
  <para>Register milter manager yum repository like the following:</para>
  <programlisting>% curl -s https://packagecloud.io/install/repositories/milter-manager/repos/script.rpm.sh | sudo bash</programlisting>
  <para>
  See also: 
  <ulink url='https://packagecloud.io/milter-manager/repos/install'>&lt;URL:https://packagecloud.io/milter-manager/repos/install&gt;</ulink>
  
</para>
  <para>Now, you install milter manager:</para>
  <programlisting>% sudo yum install -y milter-manager</programlisting>
</refsect1>

<refsect1>
  <title>Configuration</title>
  <para>Here is a basic configuration policy.</para>
  <para>
  milter-greylist should be applied only if
  <ulink url='http://gabacho.reto.jp/en/anti-spam/'>S25R</ulink>
  condition is matched to reduce needless delivery delay. But the configuration is automatically done by milter manager. You need to do nothing for it.
</para>
  <para>It&#39;s difficult that milter manager runs on SELinux. Disable SELinux policy module for Postfix and Milter.</para>
  <programlisting>% sudo semodule -d postfix
% sudo semodule -d milter</programlisting>
  <refsect2>
  <title>Configure spamass-milter</title>
  <para>At first, you configure spamd.</para>
  <para>spamd adds &quot;[SPAM]&quot; to spam mail&#39;s subject by default. If you don&#39;t like the behavior, edit /etc/mail/spamassassin/local.cf.</para>
  <para>Before:</para>
  <programlisting>rewrite_header Subject [SPAM]</programlisting>
  <para>After:</para>
  <programlisting># rewrite_header Subject [SPAM]</programlisting>
  <para>Add the following configuration to /etc/mail/spamassassin/local.cf. This configuration is for adding headers only if spam detected.</para>
  <programlisting>remove_header ham Status
remove_header ham Level</programlisting>
  <para>Start spamd on startup:</para>
  <programlisting>% sudo systemctl enable spamassassin</programlisting>
  <para>Start spamd:</para>
  <programlisting>% sudo systemctl start spamassassin</programlisting>
  <para>Here are spamass-milter&#39;s configuration items:</para>
  <itemizedlist>
  <listitem>
  <para>Disable needless body change feature.</para>
</listitem>
  <listitem>
  <para>Reject if score is larger than or equal to 15.</para>
</listitem>
</itemizedlist>
  <para>Change /etc/sysconfig/spamass-milter:</para>
  <para>Before:</para>
  <programlisting>#EXTRA_FLAGS=&quot;-m -r 15&quot;</programlisting>
  <para>After:</para>
  <programlisting>EXTRA_FLAGS=&quot;-m -r 15&quot;</programlisting>
  <para>Start spamass-milter on startup:</para>
  <programlisting>% sudo systemctl enable spamass-milter</programlisting>
  <para>Start spamass-milter:</para>
  <programlisting>% sudo systemctl start spamass-milter</programlisting>
</refsect2>

  <refsect2>
  <title>Configure clamav-milter</title>
  <para>Update ClamAV virus database and start clamd.</para>
  <para>Edit /etc/freshclam.conf like the following. It comments out &quot;Example&quot;, changes &quot;NotifyClamd&quot; value and uncomments other items.</para>
  <para>Before:</para>
  <programlisting>Example
#LogFacility LOG_MAIL
#NotifyClamd /path/to/clamd.conf</programlisting>
  <para>After:</para>
  <programlisting>#Example
LogFacility LOG_MAIL
NotifyClamd /etc/clamd.d/scan.conf</programlisting>
  <para>Run freshclam by hand at the first time:</para>
  <programlisting>% sudo freshclam</programlisting>
  <para>Configure clamd.</para>
  <para>Edit /etc/clamd.d/scan.conf like the following. It comments out &quot;Example&quot; and uncomments other items:</para>
  <para>Before:</para>
  <programlisting>Example
#LogFacility LOG_MAIL
#LocalSocket /run/clamd.scan/clamd.sock</programlisting>
  <para>After:</para>
  <programlisting>#Example
LogFacility LOG_MAIL
LocalSocket /run/clamd.scan/clamd.sock</programlisting>
  <para>Start clamd on startup:</para>
  <programlisting>% sudo systemctl enable clamd@scan</programlisting>
  <para>Start clamd:</para>
  <programlisting>% sudo systemctl start clamd@scan</programlisting>
  <para>Configure clamav-milter.</para>
  <para>Edit /etc/mail/clamav-milter.conf like the following. It comments out &quot;Example&quot;, change &quot;ClamdSocket&quot; value and uncomments other items:</para>
  <para>Before:</para>
  <programlisting>Example
#MilterSocket /run/clamav-milter/clamav-milter.socket
#MilterSocketMode 660
#ClamdSocket tcp:scanner.mydomain:7357
#LogFacility LOG_MAIL</programlisting>
  <para>After:</para>
  <programlisting>#Example
MilterSocket /run/clamav-milter/clamav-milter.socket
MilterSocketMode 660
ClamdSocket unix:/run/clamd.scan/clamd.sock
LogFacility LOG_MAIL</programlisting>
  <para>Add &quot;clamilt&quot; user to &quot;clamscan&quot; group to access clamd&#39;s socket:</para>
  <programlisting>% sudo usermod -G clamscan -a clamilt</programlisting>
  <para>Start clamav-milter on startup:</para>
  <programlisting>% sudo systemctl enable clamav-milter</programlisting>
  <para>Start clamav-milter:</para>
  <programlisting>% sudo systemctl start clamav-milter</programlisting>
</refsect2>

  <refsect2>
  <title>Configure milter-greylist</title>
  <para>Change /etc/mail/greylist.conf for the following configurations:</para>
  <itemizedlist>
  <listitem>
  <para>use the leading 24bits for IP address match to avoid Greylist adverse effect for sender uses some MTA case.</para>
</listitem>
  <listitem>
  <para>decrease retransmit check time to 10 minutes from 30 minutes (default value) to avoid Greylist adverse effect.</para>
</listitem>
  <listitem>
  <para>increase auto whitelist period to a week from 1 day (default value) to avoid Greylist adverse effect.</para>
</listitem>
  <listitem>
  <para>don&#39;t use Greylist when trusted domain passes SPF. (Trusted domains are configured in milter manager)</para>
</listitem>
  <listitem>
  <para>use Greylist by default.</para>
</listitem>
</itemizedlist>
  <note>
  <para>The configuration relaxes Greylist check to avoid Greylist adverse effect. It increases received spam mails but you should give priority to avoid false positive rather than false negative. You should not consider that you blocks all spam mails by Greylist. You can blocks spam mails that isn&#39;t blocked by Greylist by other anti-spam technique such as SpamAssassin. milter manager helps constructing mail system that combines some anti-spam techniques.</para>
</note>
  <para>Before:</para>
  <programlisting>socket &quot;/run/milter-greylist/milter-greylist.sock&quot;
# ...
racl whitelist default</programlisting>
  <para>After:</para>
  <programlisting>socket &quot;/run/milter-greylist/milter-greylist.sock&quot; 660
# ...
subnetmatch /24
greylist 10m
autowhite 1w
sm_macro &quot;trusted_domain&quot; &quot;{trusted_domain}&quot; &quot;yes&quot;
racl whitelist sm_macro &quot;trusted_domain&quot; spf pass
racl greylist sm_macro &quot;trusted_domain&quot; not spf pass
racl greylist default</programlisting>
  <para>Start milter-greylist on startup:</para>
  <programlisting>% sudo systemctl enable milter-greylist</programlisting>
  <para>Start milter-greylist:</para>
  <programlisting>% sudo systemctl start milter-greylist</programlisting>
</refsect2>

  <refsect2>
  <title>Configure milter manager</title>
  <para>Add &quot;milter-manager&quot; user to &quot;clamilt&quot; group to access clamav-milter&#39;s socket:</para>
  <programlisting>% sudo usermod -G clamilt -a milter-manager</programlisting>
  <para>Add &quot;milter-manager&quot; user to &quot;mail&quot; group and &quot;grmilter&quot; group to access milter-greylist&#39;s socket:</para>
  <programlisting>% sudo usermod -G mail -a milter-manager
% sudo usermod -G grmilter -a milter-manager</programlisting>
  <para>Add &quot;milter-manager&quot; user to &quot;postfix&quot;&quot; group to access spamass-milter&#39;s socket:</para>
  <programlisting>% sudo usermod -G postfix -a milter-manager</programlisting>
  <para>milter manager detects milters that installed in system. You can confirm spamass-milter, clamav-milter and milter-greylist are detected:</para>
  <programlisting>% sudo /usr/sbin/milter-manager -u milter-manager -g milter-manager --show-config</programlisting>
  <para>The following output shows milters are detected:</para>
  <programlisting>...
define_milter(&quot;milter-greylist&quot;) do |milter|
  milter.connection_spec = &quot;unix:/run/milter-greylist/milter-greylist.sock&quot;
  ...
  milter.enabled = true
  ...
end
...
define_milter(&quot;clamav-milter&quot;) do |milter|
  milter.connection_spec = &quot;unix:/var/run/clamav-milter/clamav-milter.socket&quot;
  ...
  milter.enabled = true
  ...
end
...
define_milter(&quot;spamass-milter&quot;) do |milter|
  milter.connection_spec = &quot;unix:/run/spamass-milter/postfix/sock&quot;
  ...
  milter.enabled = true
  ...
end
...</programlisting>
  <para>
  You should confirm that milter&#39;s name, socket path and &quot;enabled = true&quot;. If the values are unexpected, you need to change /etc/milter-manager/milter-manager.local.conf. See
  <link linkend='configuration'>Configuration</link>
   for details of milter-manager.local.conf.
</para>
  <para>But if we can, we want to use milter manager without editing miter-manager.local.conf. If you report your environment to the milter manager project, the milter manager project may improve detect method.</para>
  <para>milter manager&#39;s configuration is finished.</para>
  <para>Start to milter manager on startup:</para>
  <programlisting>% sudo systemctl enable milter-manager</programlisting>
  <para>Start to milter manager:</para>
  <programlisting>% sudo systemctl start milter-manager</programlisting>
  <para>milter-test-server is usuful to confirm milter manager was ran:</para>
  <programlisting>% sudo -u milter-manager milter-test-server -s unix:/var/run/milter-manager/milter-manager.sock</programlisting>
  <para>Here is a sample success output:</para>
  <programlisting>status: accept
elapsed-time: 0.128 seconds</programlisting>
  <para>If milter manager fails to run, the following message will be shown:</para>
  <programlisting>Failed to connect to unix:/var/run/milter-manager/milter-manager.sock</programlisting>
  <para>In this case, you can use log to solve the problem. milter manager is verbosily if --verbose option is specified. milter manager outputs logs to standard output if milter manager isn&#39;t daemon process.</para>
  <para>You can add the following configuration to /etc/sysconfig/milter-manager to output verbose log to standard output:</para>
  <programlisting>OPTION_ARGS=&quot;--verbose --no-daemon&quot;</programlisting>
  <para>Restart milter manager:</para>
  <programlisting>% sudo systemctl restart milter-manager</programlisting>
  <para>Some logs are output if there is a problem. Running milter manager can be exitted by Ctrl+c.</para>
  <para>OPTION_ARGS configuration in /etc/sysconfig/milter-manager should be commented out after the problem is solved to run milter manager as daemon process. And you should restart milter manager.</para>
</refsect2>

  <refsect2>
  <title>Configure Postfix</title>
  <para>Enables Postfix:</para>
  <programlisting>% sudo systemctl enable postfix
% sudo systemctl start postfix</programlisting>
  <para>Configure Postfix for milters. Append following lines to /etc/postfix/main.cf:</para>
  <programlisting>milter_protocol = 6
milter_default_action = accept
milter_mail_macros = {auth_author} {auth_type} {auth_authen}</programlisting>
  <para>For details for each lines.</para>
  <variablelist>
  <varlistentry>
  <term id='install-to-centos.milter-protocol'>milter_protocol = 6</term>
  <listitem>
  <para>Use milter protocol version 6.</para>
</listitem>
</varlistentry>
  <varlistentry>
  <term id='install-to-centos.milter-default-action'>milter_default_action = accept</term>
  <listitem>
  <para>MTA receives mails when MTA cannot access milter. Although there are problems between MTA and milter, MTA can deliver mails to clients. But until you recover milter, perhaps MTA receives spam mails and virus mails.</para>
  <para>If you can recover the system quickly, you can specify &#39;tempfail&#39; instead of &#39;accept&#39;. Default value is &#39;tempfail&#39;.</para>
</listitem>
</varlistentry>
  <varlistentry>
  <term id='install-to-centos.milter-mail-macros'>milter_mail_macros = {auth_author} {auth_type} {auth_authen}</term>
  <listitem>
  <para>MTA gives information related SMTP Auth to milters. milter-greylist etc. uses it.</para>
</listitem>
</varlistentry>
</variablelist>
  <para>Register milter manager to Postfix.  It&#39;s important that spamass-milter, clamav-milter and milter-greylist aren&#39;t needed to be registered because they are used via milter manager.</para>
  <para>Append following lines to /etc/postfix/main.cf:</para>
  <programlisting>smtpd_milters = unix:/var/run/milter-manager/milter-manager.sock</programlisting>
  <para>Reload Postfix&#39;s configuration.</para>
  <programlisting>% sudo systemctl reload postfix</programlisting>
  <para>milter manager logs to syslog. If milter manager works well, some logs can be shown in /var/log/maillog. You need to send a test mail for confirming.</para>
</refsect2>

</refsect1>

<refsect1>
  <title>Conclusion</title>
  <para>There are many configurations to work milter and Postfix together. They can be reduced by introducing milter manager.</para>
  <para>Without milter manager, you need to specify sockets of spamass-milter, clamav-milter and milter-greylist to /etc/postfix/main.cf. With milter manager, you don&#39;t need to specify sockets of them, just specify a socket of milter manager. They are detected automatically. You don&#39;t need to take care some small mistakes like typo.</para>
  <para>milter manager also detects which &#39;/sbin/chkconfig -add&#39; is done or not. If you disable a milter, you use the following steps:</para>
  <programlisting>% sudo systemctl stop milter-greylist
% sudo systemctl disable milter-greylist</programlisting>
  <para>You need to reload milter manager after you disable a milter.</para>
  <programlisting>% sudo systemctl reload milter-manager</programlisting>
  <para>milter manager detects a milter is disabled and doesn&#39;t use it. You don&#39;t need to change /etc/postfix/main.cf.</para>
  <para>You can reduce maintainance cost by introducing milter manager if you use some milters on CentOS.</para>
  <para>
  milter manager also provides tools to help operation. Installing them is optional but you can reduce operation cost too. If you also install them, you will go to
  <link linkend='install-options-to-centos'>Install to CentOS (optional)</link>
  .
</para>
</refsect1>
</refentry>
